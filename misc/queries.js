const createDeterminationTable = `
  DO
  $$
  BEGIN
      IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname = 'syncuser') THEN
          CREATE USER syncuser;
      END IF;
  END
  $$;

 DROP TABLE IF EXISTS nsl_determination;
 CREATE TABLE nsl_determination (
      "veg_barcode" text UNIQUE NOT NULL,
      "taxa_id" varchar,
      "tax_family" varchar,
      "tax_genus" varchar,
      "tax_specific_epithet" varchar,
      "tax_infraspecific_rank" varchar,
      "tax_infraspecific_epithet" varchar,
      "tax_status" varchar,
      "original_herbarium_determination" varchar NOT NULL,
      "last_updated" date NOT NULL,
      "scientific_name_authorship" varchar,
      "scientific_name_published_in" varchar,
      "scientific_name" varchar,
      "standardised_scientific_name" varchar,
      "taxon_rank" varchar,
      "tax_group" varchar,
      "kingdom" varchar
  )
 `

const grantSelect =
  'GRANT SELECT ON ALL TABLES IN SCHEMA public TO syncuser;\
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO syncuser;'

const getAllDets = `
SELECT
  hd.herbarium_determination,
  hd.veg_barcode
FROM
  herbarium_determination hd
WHERE
  hd.determiner notnull
  AND TRIM(hd.determiner) NOT IN ('', 'N/A')
  AND hd.veg_barcode NOT LIKE '%NO_BARCODE%'
  AND LOWER(hd.veg_barcode) NOT IN (
    'not collected',
    'delete',
    'missing specimen',
    'null',
    'indeterminate',
    'dead tree/shrub',
    'annual forb'
  )
  AND LOWER(hd.herbarium_determination) NOT IN (
    'not collected',
    'delete',
    'missing specimen',
    'null',
    'indeterminate',
    'dead tree/shrub',
    'annual forb',
    'no id',
    'id pending from victoria herbarium',
    'inadequate',
    'id pending',
    'undefined'
  )`

const detsFromBarcode =
  'SELECT herbarium_determination, veg_barcode FROM herbarium_determination WHERE veg_barcode = ANY($1)'

/**
 *
 * @param {Array} columns
 */
const bulkInsertToDetQuery = (columnsNames) => {
  // console.log(columnsNames);
  const stringifiedColumns = columnsNames.reduce((acc, current, index) => {
    if (index === 0) return '"' + current + '"'
    return acc + ', "' + current + '"'
  }, '')
  const valuesString = columnsNames.reduce((acc, _, index) => {
    if (index === 0) {
      return '$' + (index + 1)
    }
    return acc + ', $' + (index + 1)
  }, '')
  const query = `INSERT INTO nsl_determination(${stringifiedColumns})\
  VALUES(${valuesString})
  `
  return query
}

module.exports = {
  createDeterminationTable,
  getAllDets,
  bulkInsertToDetQuery,
  grantSelect,
  detsFromBarcode,
}
