# swarm-nsl-dets-bot

## Prerequisites

You need docker engine or nodejs to this script
## Installation and Usage

* Without docker
1. Clone this repo
1. Rename example.env to .env
1. Specify environment variables in .env file, 
    - Client ID and client secret are obtained from the json file when you setup credential for your project on google cloud
    - Refresh token is obtained from https://developers.google.com/oauthplayground/, 
    - Click "Configuration", then tick "Use your own OAuth credentials" and enter the same client ID and client secret
    - Press 'Authorize API' button
    - In step 2, click "Exchange authorization code for tokens" and the Refresh token will be generated, it may jump to step 3 but you can go back to step 2 and copy it, for google cloud project in testing mode, this token may expire after 7 days, so you need to repeat the above steps again. Otherwise, in production mode you only need to get Refresh Token once.
![Screenshot](./misc/screenshot.jpg)
```bash
DBUSER=<database user name>
PASSWORD =<database password>
HOST=localhost
DATABASE=<database name>
CLIENT_ID=<client ID from goole API>
CLIENT_SECRET=<client secret from goole API>
REFRESH_TOKEN=<refresh token from goole API>
USER_EMAIL=<google API registered email also sender email>
RECIPIENT=<email of person who receives weekly report>
```

* Run the script in a container 
  - Download the docker-compose.yml file in from this repo, specify the env variable similar to the instruction above, and run <code>docker compose up </code> or <code>docker-compose up</code>, depends on your docker version

## NSL api information and contacts

author: Mo Ziauddin

email: moziauddin@gmail.com

production endpoint : https://api.biodiversity.org.au/name/check

test endpoint : https://test-api.biodiversity.org.au/name/check
## Authors and acknowledgment
Huy Vo

## License
For open source projects, say how it is licensed.

