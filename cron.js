const init = require('./index')
const cron = require('node-schedule')

require('dotenv').config()
// cron job
let startTime = process.env.CRON_START_TIME || 4 // am
// console.log('start cron', new Date())
if (parseInt(startTime) > 23) {
  console.log('invalid cron start time, default to 4 am')
  startTime = 4
}
cron.scheduleJob(`0 ${parseInt(startTime)} * * * `, async () => {
  // This function will run every Monday
  console.log(`Running script at ${startTime}:00 everyday!`)
  console.log(new Date())
  await init()
})