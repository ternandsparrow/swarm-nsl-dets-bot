/* eslint-disable camelcase */
const { Pool } = require('pg')
const fs = require('fs/promises')
const axios = require('axios')
const sendMail = require('./sendMail')
require('dotenv').config()
const { setTimeout } = require('timers/promises')

const credentials = {
  user: process.env.DBUSER,
  password: process.env.PASSWORD,
  host: process.env.HOST,
  port: process.env.PORT,
  database: process.env.DATABASE
}

const pool = new Pool(credentials)

if (require.main === module) {
  init()
}
module.exports = init

let determinationData
/**
 * make query to database
 * @param {string} query SQL query
 * @returns
 */
async function dbQuery (query, data) {
  try {
    const res = await pool.query(query, data ? [data] : undefined)
    return res.rows
  } catch (error) {
    console.log(error)
    throw new Error('Query to data failed')
    // process.exit(1);
  }
}
async function getDeterminations () {
  const { getAllDets } = require('./misc/queries')
  // get all the determinations
  determinationData = await dbQuery(getAllDets)

  determinationData = determinationData.map(
    ({ herbarium_determination, ..._ }) => {
      // drop "sp" or "sp."
      herbarium_determination = herbarium_determination
        .replace(/\bsp\.?\b/g, '')
        // drop weird white space character and replace with normal one
        .replace(/\p{Zs}|[^\S\r\n]/gu, ' ')
        .trim()
      return {
        ..._,
        herbarium_determination
      }
    }
  )
  // remove duplicates, we have over 54k dets but actually just around 7k names
  const determinations = [
    ...new Set(
      determinationData.map(
        ({ herbarium_determination }) => herbarium_determination
      )
    )
  ]
  return removeFungi(determinations)
}

/**
 * looping over species name and get data from NSL
 * @param {Array} speciesNames list of species names from herbarium_determination table
 * @param {Integer} endPoint index of species name to end fetching
 * @returns
 */
async function getAllDataFromNSL (speciesNames) {
  console.time('fetching')
  let data = []
  // Too much names at the same time may have longer fetching time
  // or causing request timeout
  const step = 300
  console.log('fetching data from NSL api...')

  for (let index = 0; index < speciesNames.length; index += step) {
    const request = speciesNames.slice(index, index + step)
    try {
      const results = await fetchData(request)

      console.log(
        `downloaded ${index + results.length} records over ${speciesNames.length
        }`
      )
      data = data.concat(results)
      await setTimeout(5000)
    } catch (error) {
      console.log(
        `Failed to download data for ${JSON.stringify(request)} from NSL`
      )
      throw error
    }
  }
  console.info('Done!')
  console.timeEnd('fetching')
  // write to an file, maybe we can reuse it, instead wasting time to wait for the data to be downloaded
  if (process.env.DEV) {
    fs.writeFile('data.json', JSON.stringify(data), 'utf-8')
  }
  return data
}
/**
 *
 * @param {Array} data
 * @param {Integer} maxRetries
 * @param {Number} retryDelay
 * @returns
 */
async function fetchData (data, maxRetries = 3, retryDelay = 5000) {
  let retries = 0
  while (retries < maxRetries) {
    try {
      if (retries > 0) {
        console.log('re-downloading...')
      }
      const { results } = await doPost(data)
      // handle response data
      return results
    } catch (error) {
      console.error(error)
      retries++
      await new Promise((resolve) => setTimeout(resolve, retryDelay))
    }
  }
  throw new Error(`Max retries (${maxRetries}) exceeded`)
}
async function doPost (request) {
  const url = 'https://api.biodiversity.org.au/name/check'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  try {
    const { data } = await axios.post(url, { names: request }, config)
    return data
  } catch (error) {
    throw new Error(
      'Request to NSL API failed with status:',
      error.response.status
    )
  }
}

function checkData (fetchedData) {
  let validData = []
  const noMatch = []
  const isFungi = []
  const dups = []
  const count = {}
  fetchedData.forEach((item) => {
    if (!count[item.verbatimSearchString]) {
      count[item.verbatimSearchString] = 1
    } else {
      count[item.verbatimSearchString]++
      dups.push(item.verbatimSearchString)
    }
    const relatedRecord = determinationData.filter(
      (record) =>
        record.herbarium_determination.trim() ===
        item.verbatimSearchString.trim()
    )

    if (!item.results?.length) {
      // find all barcode that have the same det

      const noMatchItemData = {
        herbarium_determination: item.verbatimSearchString,
        barcode: relatedRecord.map(({ veg_barcode }) => veg_barcode).join(',')
      }
      noMatch.push(noMatchItemData)
      relatedRecord.forEach(({ veg_barcode }) => {
        validData.push({
          veg_barcode,
          original_herbarium_determination: item.verbatimSearchString,
          last_updated: new Date()
        })
      })
      return
    }

    const plantResults = item.results.filter(
      (result) =>
        result.datasetName !== 'AFD' &&
        result.datasetName !== 'AFNI' &&
        result.datasetName !== 'ALNI'
    )

    if (plantResults.length === 0) {
      const noMatchItemData = {
        herbarium_determination: item.verbatimSearchString,
        barcode: relatedRecord.map(({ veg_barcode }) => veg_barcode).join(',')
      }
      isFungi.push(noMatchItemData)
    } else {
      const { rankFullName: taxonRank } = extractInfraspecific(item.verbatimSearchString)

      let firstAccepted = plantResults.find((item) => {
        if (taxonRank) {
          return item.resultNameMatchStatus === 'accepted' && item.taxonRank === taxonRank
        }
        return item.resultNameMatchStatus === 'accepted'
      })

      if (!firstAccepted) {
        firstAccepted = plantResults.find(
          (item) => {
            if (taxonRank) {
              return item.resultNameMatchStatus === 'included' && item.taxonRank === taxonRank
            }
            return item.resultNameMatchStatus === 'included'
          }
        )
      }

      if (!firstAccepted) {
        if (taxonRank) {
          firstAccepted = plantResults.find(
            (item) => {
              return item.taxonRank === taxonRank
            }
          )
        } else {
          firstAccepted = plantResults[0]
        }
      }
      if (!firstAccepted) {
        const noMatchItemData = {
          herbarium_determination: item.verbatimSearchString,
          barcode: relatedRecord.map(({ veg_barcode }) => veg_barcode).join(',')
        }
        noMatch.push(noMatchItemData)
        relatedRecord.forEach(({ veg_barcode }) => {
          validData.push({
            veg_barcode,
            original_herbarium_determination: item.verbatimSearchString,
            last_updated: new Date()
          })
        })
        return
      }
      const data = formatValidData(firstAccepted, item.verbatimSearchString)
      validData = validData.concat(data)
    }
  })
  dups.forEach((dup) => console.log(dup, count[dup]))
  return { validData, noMatch, isFungi }
}

async function exportToCsv (name, data, append = false) {
  const convertedData = convertToCSV(data)
  try {
    if (!append) {
      await fs.writeFile(`${name}.csv`, convertedData)
    } else fs.appendFile(`${name}.csv`, convertedData)
  } catch (error) {
    // console.log(error);
    console.log(`failed to export ${name} to csv`)
    throw new Error(`failed to export ${name} to csv with error msg:`, error)
  }
}

function convertToCSV (data) {
  if (!data.length) return []
  const csvRows = []

  // Get the headers from the data object
  const headers = Object.keys(data[0])

  // Add the headers to the CSV string
  csvRows.push(headers.join(','))

  // Loop through the data objects and add them to the CSV string
  for (const row of data) {
    const values = headers.map((header) => {
      const escaped = ('' + row[header]).replace(/"/g, '\\"')
      return `"${escaped}"`
    })
    csvRows.push(values.join(','))
  }

  // Join all the rows into a single CSV string
  return csvRows.join('\n')
}
function formatValidData (rawData, originalDet, taxonRank) {
  const current = new Date()

  const infraspecific = extractInfraspecific(rawData.canonicalName)

  // some returned data have multiple entries inside hasUsage prop
  const hasUsage = rawData.hasUsage || []

  let acceptedUsage = hasUsage.find(
    (usage) => usage.taxonomicStatus === 'accepted'
  )

  if (!acceptedUsage && hasUsage.length > 0) {
    acceptedUsage = hasUsage[0]
  }
  const scientific_name =
    acceptedUsage?.hasAcceptedName.canonicalName || rawData.canonicalName
  const tax_specific_epithet = scientific_name.split(' ')[1]
  const genus = scientific_name.split(' ')[0]

  const tax_group =
    acceptedUsage?.hasAcceptedName.higherClassification.split('|')[2]
  const formattedData = {
    // veg_barcode: determinationData[index]['veg_barcode'],
    tax_family: acceptedUsage?.hasAcceptedName.family || null,
    tax_genus: genus,
    tax_specific_epithet,
    tax_infraspecific_rank: !infraspecific ? null : infraspecific.rank,
    tax_infraspecific_epithet: !infraspecific ? null : infraspecific.epithet,
    tax_status: acceptedUsage?.taxonomicStatus || rawData.taxonomicStatus,
    original_herbarium_determination: originalDet,
    last_updated: current,
    scientific_name_authorship:
      acceptedUsage?.hasAcceptedName.scientificNameAuthorship ||
      rawData.scientificNameAuthorship,
    scientific_name_published_in:
      acceptedUsage?.hasAcceptedName.scientificNameID || null,
    scientific_name,
    taxon_rank: acceptedUsage?.hasAcceptedName.taxonRank || rawData.taxonRank,
    taxa_id:
      acceptedUsage?.hasAcceptedName.scientificNameID ||
      rawData.scientificNameID,
    standardised_scientific_name:
      acceptedUsage?.hasAcceptedName.scientificName || rawData.scientificName,
    kingdom: 'Plantae',
    tax_group: tax_group || null
  }

  // find all barcode that have the same det
  const barcode = determinationData.filter(
    (record) => record.herbarium_determination.trim() === originalDet.trim()
  )

  // clone the standardised name to all the matched barcode
  const validData = []
  barcode.forEach((item) => {
    validData.push({ ...formattedData, veg_barcode: item.veg_barcode })
  })

  return validData
}

/**
 * Extract infraspecific_rank and infraspecific_epithet from scientific name,
 * as the name not always have two of them
 */
function extractInfraspecific (canonicalName) {
  if (!canonicalName) return
  // got this from Emrys
  const ranks = ['subsp.', 'var.', 'f.']
  const nameArray = canonicalName.split(' ')
  const rank = nameArray.find((word) => ranks.includes(word))

  if (!rank) return false
  const epithetIndex = nameArray.findIndex((word) => word === rank) + 1
  const epithet = nameArray[epithetIndex]

  const rankMap = {
    'subsp.': 'Subspecies', 'var.': 'Varietas', 'f.': 'Forma'
  }

  return { rank, epithet, rankFullName: rankMap[rank] }
}

async function insertDataToDb (data) {
  // start a transaction
  const client = await pool.connect()
  try {
    await client.query('BEGIN')
    // create table if not exist
    console.log('Creating table....')
    const {
      createDeterminationTable,
      bulkInsertToDetQuery,
      grantSelect
    } = require('./misc/queries')

    await client.query(createDeterminationTable)
    // grant select to syncuser
    await client.query(grantSelect)
    // insert each record to det table
    console.log('Inserting data...')
    for (const item of data) {
      await client.query(
        bulkInsertToDetQuery(Object.keys(item)),
        Object.values(item)
      )
    }
    await client.query('COMMIT')
    console.log('Done!')
  } catch (error) {
    await client.query('ROLLBACK')
    console.log('failed to inserting data to database')
    console.log(error)
    throw new Error('failed to inserting data to database')
  } finally {
    client.release()
  }
}

function removeFungi (data) {
  const fungiList = require('./misc/fungi').map((_) => _.toLowerCase())
  const processedData = data.filter(
    (determination) => !fungiList.includes(determination.toLowerCase())
  )
  return processedData
}

async function init () {
  try {
    const { createDeterminationTable } = require('./misc/queries')
    await dbQuery(createDeterminationTable)
    const determinations = await getDeterminations()
    // fs.writeFile('dets.json', JSON.stringify(determinations, null, 4))
    const data = await getAllDataFromNSL(determinations)
    // await fs.writeFile('data.json', JSON.stringify(data, null, 2))
    // const data = JSON.parse(await fs.readFile('data.json', 'utf-8'))
    const { validData, noMatch, isFungi } = checkData(data)

    await insertDataToDb(validData)
    await exportToCsv('./no_match', noMatch)
    await exportToCsv('./is_fungi', isFungi)

    console.log('Successfully updated ' + validData.length + ' determinations ')
    console.log(noMatch.length + " names didn't match")
    await sendMail()
  } catch (error) {
    await sendMail(error)
    console.error(error)
    process.exit(1)
  }
}
