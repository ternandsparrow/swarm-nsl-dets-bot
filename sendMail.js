const nodemailer = require('nodemailer')
const { google } = require('googleapis')
require('dotenv').config()

const auth = {
  type: 'OAuth2',
  user: process.env.USER_EMAIL,
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  refreshToken: process.env.REFRESH_TOKEN
}

const defaultMailOptions = {
  from: `ausplot <${process.env.USER_EMAIL}>`,
  to: `${process.env.RECIPIENT}`,
  subject: 'Weekly NSL Determination Update'
}

const oAuth2Client = new google.auth.OAuth2(
  process.env.CLIENT_ID,
  process.env.CLIENT_SECRET,
  process.env.REDIRECT_URI
)

oAuth2Client.setCredentials({ refresh_token: process.env.REFRESH_TOKEN })

module.exports = async function sendMail (error = null) {
  if (!process.env.CLIENT_ID) return
  console.log('Sending email...')
  // console.log(typeof error);
  try {
    const accessToken = await oAuth2Client.getAccessToken()
    const transport = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        ...auth,
        accessToken
      }
    })
    // console.log(error);
    let mailOptions
    if (!error) {
      mailOptions = {
        ...defaultMailOptions,
        text: 'NSL_determination Update Results',
        attachments: [{ path: './no_match.csv' }]
      }
    } else {
      defaultMailOptions.subject = 'Weakly NSL update failed'
      mailOptions = {
        ...defaultMailOptions,
        text: error
      }
    }

    // console.log(mailOptions);
    await transport.sendMail(mailOptions)
    // console.log(result);
    console.log('Done!')
  } catch (error) {
    console.log(error)
  }
}
