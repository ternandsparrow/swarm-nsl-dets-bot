FROM node:18-slim as swarm-snl-dets-bot

# Set the timezone to Adelaide Central Time
ENV TZ Australia/Adelaide
# Create app directory
WORKDIR /app
COPY . .
RUN yarn install --production
CMD [ "node", "index.js" ]


